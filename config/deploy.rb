set :application, "Cap_BLOG"
set :repository,  "git@github.com:Chongsawad/Blog.git"
set :deploy_to, '/home/inice/InstantSOA/www/blog_cap/'

set :scm, "git"
set :user, "chongsawad252@msn.com"
set :ssh_options, { :forward_agent => true }
set :password, 'kckctv9t2925'
set :use_sudo, false

# Or: `accurev`, `bzr`, `cvs`, `darcs`, `git`, `mercurial`, `perforce`, `subversion` or `none`

role :web, "inice@inice"                          # Your HTTP server, Apache/etc
role :app, "inice@inice"                         # This may be the same as your `Web` server
role :db,  "inice@inice", :primary => true # This is where Rails migrations will run

# If you are using Passenger mod_rails uncomment this:
# if you're still using the script/reapear helper you will need
# these http://github.com/rails/irs_process_scripts

namespace :deploy do
  desc "Restarting mod_rails with restart.txt"
  task :restart, :roles => :app, :except => { :no_release => true } do
    run "mkdir -p #{shared_path}/log"
    run "mkdir -p #{shared_path}/pids"
  end
end

