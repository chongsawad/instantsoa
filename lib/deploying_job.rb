class DeployingJob

  def initialize(user,application,site)
    puts "Initialize Job Detail"
    @app_name = user.username + '_' + site.name 
    @app_path = application.path
    @siteId = site.id
    @path = site.path

    puts "Write Nginx Configuration to conf_domain_path"
    f = File.new("#{RAILS_ROOT}/conf_domain/#{@app_name}.conf","w") 
    @server_conf= <<-EOF 
#------- Begin Sub-Domain site_id=#{@siteId}  -------#

  server {
    listen		80;
    server_name 	#{@app_name}.local;
    root  		#{@path}/current/public;
    passenger_enabled 	on;
  }

#------------------ End Sub-Domain -------------------#"
EOF
    f.puts @server_conf
    f.close

  end

  def perform
    puts "Deploying Job"
    system("sh -c 'cd #{@app_path}/; cap deploy APPNAME=#{@app_name};'")
  end
end

