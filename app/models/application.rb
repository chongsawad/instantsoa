class Application < ActiveRecord::Base
  validates_presence_of :name
  has_many :sites
end
