class HomeController < ApplicationController

  def index
    @applications = Application.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @application }
    end
  end


  def show
    @application = Application.find(params[:id])
  end

end
