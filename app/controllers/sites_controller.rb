class SitesController < ApplicationController
  before_filter :authenticate_user!

  def index
    @site = Site.all(:joins => "LEFT OUTER JOIN applications ON sites.id = applications.id where sites.user_id='#{current_user.id}'")
  end

  def show 
    @site = Site.find(params[:id])
  end

  def new
    @application = Application.find(params[:application_id])
    @site = Site.new
  end

  def create

    @application = Application.find(params[:application_id])
    @site = Site.new(params[:site])
    @site.path = "/home/inice/InstantSOA/deploy_cap/inice/#{current_user.username}_#{@site.name}"

    if @site.save
      #UserMailer.deliver_mail_form
      Delayed::Job.enqueue(DeployingJob.new(current_user, @application, @site))

      redirect_to payment_index_path 
    else render :action => "new"

    end

  end

  def edit
    @site = Site.find(params[:id])

  end

  def update
    @site = Site.find(params[:id])

    respond_to do |format|
      if @site.update_attributes(params[:site])
        flash[:notice] = 'Application was successsfully updated.'
        format.html { redirect_to(@site) }
        format.xml { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml { render :xml => @site.errors, :status => :unprocessable_entity }
      end
    end
  end

  def destroy
    @site = Site.find(params[:id])
    @site.destroy

    respond_to do |format|
      format.html { redirect_to(sites_url) }
      format.xml { head :ok }
    end
  end
end

