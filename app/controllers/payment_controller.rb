class PaymentController < ApplicationController
  def index 

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @payment }
    end
  end

  def show 
    @payment = Payment.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @payment }
    end
  end

end
