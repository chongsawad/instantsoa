class CreateApplications < ActiveRecord::Migration
  def self.up
    create_table :applications do |t|
      t.string :name
      t.string :content
      t.string :img_path
      t.string :path

      t.timestamps
    end
  end

  def self.down
    drop_table :applications
  end
end
