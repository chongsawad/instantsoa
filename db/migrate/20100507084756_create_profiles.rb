class CreateProfiles < ActiveRecord::Migration
  def self.up
    create_table :profiles do |t|
      t.column :user_id, :integer
      t.string :firstname
      t.string :lastname
      t.column :personal_id, :integer, :limit => 4
      t.string :email
      t.string :phone

      t.timestamps
    end
  end

  def self.down
    drop_table :profiles
  end
end

