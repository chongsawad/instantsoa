class CreateSites < ActiveRecord::Migration
  def self.up
    create_table :sites do |t|
      t.string :name
      t.column :app_id, :integer, :null => false
      t.column :user_id, :integer, :null => false
      t.column :db_size, :integer
      t.string :path
      t.string :payment_approve

      t.timestamps
    end
  end

  def self.down
    drop_table :sites
  end
end
